package hr.ferit.bruno.booksie;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import java.util.List;

import hr.ferit.bruno.booksie.model.Book;
import hr.ferit.bruno.booksie.persistance.BookGenerator;
import hr.ferit.bruno.booksie.persistance.room.BookDao;
import hr.ferit.bruno.booksie.persistance.room.BookDatabase;

/**
 * Created by Stray on 3.4.2018..
 */

public class BookRepository {

	private static BookRepository sInstance;

	private BookDatabase mDatabase;
	private LiveData<List<Book>> mData;

	private BookRepository(Application application){
		mDatabase = BookDatabase.getInstance(application);
		mData = mDatabase.bookDao().getBooks();
	}

	public static BookRepository getInstance(){
		if(sInstance == null){
			sInstance = new BookRepository(BooksieApp.getInstance());
		}
		return sInstance;
	}

	public LiveData<List<Book>> getBooks(){
		return mData;
	}

	public void insertBook() {
		// Use a separate thread to access the database since it could take a while.
		Book book = BookGenerator.generate();
		new insertBookAsyncTask(mDatabase.bookDao()).execute(book);
	}

	public void deleteBook(Book book) {
		new deleteBookTask(mDatabase.bookDao()).execute(book);
	}

	private class insertBookAsyncTask extends AsyncTask<Book,Void,Void>{

		private BookDao mBookDao;

		public insertBookAsyncTask(BookDao bookDao) {
			mBookDao = bookDao;
		}

		@Override
		protected Void doInBackground(Book... books) {
			mBookDao.insertBook(books[0]);
			return null;
		}
	}

	private class deleteBookTask extends AsyncTask<Book, Void, Void>{
		private BookDao mBookDao;

		public deleteBookTask(BookDao bookDao) {
			this.mBookDao = bookDao;
		}

		@Override
		protected Void doInBackground(Book... books) {
			mBookDao.deleteBook(books[0]);
			return null;
		}
	}
}
