package hr.ferit.bruno.booksie.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Book {

    @PrimaryKey private int mId;
    @ColumnInfo(name = "title") private String mTitle;
    @ColumnInfo(name = "author") private String mAuthor;
    @ColumnInfo(name = "imageUri") private String mImageUrl;

    public Book(int id, String title, String author, String imageUrl) {
        mId = id;
        mTitle = title;
        mAuthor = author;
        mImageUrl = imageUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Book{" +
                "mTitle='" + mTitle + '\'' +
                ", mAuthor='" + mAuthor + '\'' +
                ", mImageUrl='" + mImageUrl + '\'' +
                '}';
    }

    public int getId() {
        return mId;
    }
}
