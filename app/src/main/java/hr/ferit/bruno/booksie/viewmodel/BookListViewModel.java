package hr.ferit.bruno.booksie.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import hr.ferit.bruno.booksie.BookRepository;
import hr.ferit.bruno.booksie.model.Book;

/**
 * Created by Stray on 3.4.2018..
 */

public class BookListViewModel extends ViewModel{

	BookRepository mRepository;

	public BookListViewModel() {
		mRepository = BookRepository.getInstance();
	}

	public LiveData<List<Book>> getBookList(){
		return mRepository.getBooks();
	}

	public void insertBook(){
		mRepository.insertBook();
	}

	public void deleteBook(Book book){
		mRepository.deleteBook(book);
	}
}
