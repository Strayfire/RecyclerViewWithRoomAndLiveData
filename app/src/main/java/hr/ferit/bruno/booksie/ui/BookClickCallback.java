package hr.ferit.bruno.booksie.ui;

import hr.ferit.bruno.booksie.model.Book;

public interface BookClickCallback {
    void onClick(Book book);
    boolean onLongClick(Book book);
}
