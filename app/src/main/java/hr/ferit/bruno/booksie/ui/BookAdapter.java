package hr.ferit.bruno.booksie.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import hr.ferit.bruno.booksie.R;
import hr.ferit.bruno.booksie.model.Book;

class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    private List<Book> mBooks;
    private BookClickCallback mCallback;

    public BookAdapter(List<Book> books, BookClickCallback onBookClickListener) {
        mBooks = new ArrayList<>();
        this.refreshData(books);
        mCallback = onBookClickListener;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_book, parent, false);
        return new BookViewHolder(view, mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book current = mBooks.get(position);
        holder.tv_itemBook_title.setText(current.getTitle());
        holder.getTv_itemBook_author.setText(current.getAuthor());
        Picasso.get()
                .load(current.getImageUrl())
                .centerCrop()
                .fit()
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.iv_itemBook_cover);
    }

    @Override
    public int getItemCount() {
        return mBooks.size();
    }

    public void refreshData(List<Book> books) {
        mBooks.clear();
        mBooks.addAll(books);
        this.notifyDataSetChanged();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_itemBook_cover) ImageView iv_itemBook_cover;
        @BindView(R.id.tv_itemBook_title) TextView tv_itemBook_title;
        @BindView(R.id.tv_itemBook_author) TextView getTv_itemBook_author;

        BookClickCallback mCallback;

        public BookViewHolder(View itemView, final BookClickCallback callback) {
            super(itemView);
            mCallback = callback;
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        public void onBookClick(){
            mCallback.onClick(mBooks.get(getAdapterPosition()));
        }

        @OnLongClick
        public boolean onBookLongClick(){
            return mCallback.onLongClick(mBooks.get(getAdapterPosition()));
        }
    }
}
