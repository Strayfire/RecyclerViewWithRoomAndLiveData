package hr.ferit.bruno.booksie.persistance.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import hr.ferit.bruno.booksie.model.Book;

/**
 * Created by Stray on 3.4.2018..
 */

@Dao
public interface BookDao {

	@Query("SELECT * FROM Book")
	LiveData<List<Book>> getBooks();

	@Query("SELECT * FROM Book WHERE author = :author")
	LiveData<List<Book>> getBooksBy(String author);

	@Delete
	void deleteBook(Book book);

	@Insert
	void insertBook(Book book);
}
