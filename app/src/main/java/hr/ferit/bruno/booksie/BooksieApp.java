package hr.ferit.bruno.booksie;

import android.app.Application;
import android.content.Context;

/**
 * Created by Stray on 3.4.2018..
 */

public class BooksieApp extends Application {

	private static BooksieApp sInstance;

	public static BooksieApp getInstance(){
		return sInstance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		sInstance = this;
	}


}
