# Booksie project

An example app demonstrating the use of a RecyclerView, its event handling and a Room database.
You can work with the hashses by creating a new branch and deleting it after you studied the code and the state of the project.

For example, to see the project at the initial commit you could:

git checkout -b test-branch df3f8 
// have a look around the code.

git checkout \<branch-name\>

git branch -d test-branch

You can also just checkout the commit, but it will get your HEAD detached. 
For any of the aforementioned you can use the provided tags.


### Tag:Initial (df3f8)

Created the initial empty project with a single Activity and its layout. Branched out to RecyclerViewDemo. 

## RecyclerViewDemo

### Tag:Dependencies (99790)

We will be using Butterknife, Picasso and RecyclerView for this project. Added the dependencies and extracted the versions to the gradle build files.

### Tag:Step1 (84c43)

Added the book class and the activitys layout. Set up packages.

### Tag:Step2 (952f8)

Added the layout for a single book item and a fake book generator class.

### Tag:Step3 (efcf9)

Added setup code for the RecyclerView in the Activity, but the adapter is not yet done. Compile time errors.

### Tag:Step4 (d3f4d)

Added the BookAdapter adapter class. Fixed the orientation of the layout and the cover dimensions.

### Tag:Step5 (bb70c) 

Added custom on click and on long click behavior - display a book and delete a book. Added a FAB to insert new books. 

### Tag:Step6 (1bcf85)

Added the support for LiveData and ViewModel, added the Repository class for later use with database.

### Tag:Step7 (a8162)

Added Room database. Changed Book class to be an Entity, added Book database access object and the database created as a singleton.

### Tag:Step8 (66624e)

Added background processing for database communication.


